http://www.regexr.com/

# Timestamp Regex
`\(\d{1,2}\:\d{1,2}\:\d{1,2} (A|P)M\)`
<br />Matches: Pidgin timestamps

`\d{1,2}\/\d{1,2}/\d{1,4}`
<br />Matches: 10/6/2015

`\d{1,2}\/\d{1,2}/\d{1,4} \d{1,2}\:\d{1,2}`
<br />Matches: 10/6/2015 10:25

`\d{1,4}\-\d{1,2}-\d{1,2} \d{1,2}\:\d{1,2}:\d{1,2}`
<br />Matches: 2015-09-19 12:44:03

`\d{1,2}\:\d{1,2}\:\d{1,2}`
<br />Matches: 00:00:00

`([A-Z])\w+ ([A-Z])\w+`
<br />Matches: `Jeannie Walters`

`([A-Z])\w+ ([A-Z])\w+`|`([a-z])\w+-([a-z])\w+`
<br />Matches: `Jeannie Walters` OR `jeannie walters`

`([a-z])\w+-([a-z])\w+\@www.\w+.com`|`http:\/\/\w+.\w+.com
<br />Matches: `jeannie-walters@www.business2community.com` OR `http://www.business2community.com`

`http:\/\/\w+.\w+.com\/author\/\w+-\w+`
<br />Matches: `http://www.business2community.com/author/jodi-schechter`

`https\:\/\/cisofy.com\/([a-z]+)\/([A-Z]+)-([0-9]+)\/`
<br />Matches: https://cisofy.com/controls/STRG-1840/

**Grab stuff between double quotes followed by a colon**
\".*?\":
<br />Matches: "http://jplpdb.premiereglobal.co.jp/qwest/static/":

**Clean up Nmap Grepable Output**
`Host: (?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}?(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?) \(\)\tStatus: Up`
<br />Matches: Host: 10.32.0.1 ()	Status: Up

[(0-9)]\w+\/closed\/tcp\/\/[A-Z]\w+:[0-9]\/\/\/,|[(0-9)]\w+\/closed\/tcp\/\/[A-Z]\w+[0-9]\/\/\/,
Matches: 6000/closed/tcp//X11///, 6001/closed/tcp//X11:1///, 6002/closed/tcp//X11:2///, 


## IPv4 Regex

7.16. Matching IPv4 Addresses
Problem

You want to check whether a certain string represents a valid IPv4 address in 255.255.255.255 notation. Optionally, you want to convert this address into a 32-bit integer.
Solution
Regular expression

Simple regex to check for an IP address:

^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$

Regex options: None
Regex flavors: .NET, Java, JavaScript, PCRE, Perl, Python, Ruby

Accurate regex to check for an IP address:

^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}?
(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$

Regex options: None
Regex flavors: .NET, Java, JavaScript, PCRE, Perl, Python, Ruby

Simple regex to extract IP addresses from longer text:

\b(?:[0-9]{1,3}\.){3}[0-9]{1,3}\b

Regex options: None
Regex flavors: .NET, Java, JavaScript, PCRE, Perl, Python, Ruby

Accurate regex to extract IP addresses from longer text:

\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}?
(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b

Regex options: None
Regex flavors: .NET, Java, JavaScript, PCRE, Perl, Python, Ruby

Simple regex that captures the four parts of the IP address:

^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$

Regex options: None
Regex flavors: .NET, Java, JavaScript, PCRE, Perl, Python, Ruby

Accurate regex that captures the four parts of the IP address:

```
^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.?
(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.?
(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.?
(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$
```

## Other Neat stuff

Trim to 8 characters
^([^\n]{8}).*?$

Replace with
$1

**Replace All Newlines**
(\n|\r|\r\n)

*Convert CaMeLCaSe to snake_case*
```
(\b[a-z]+|\G(?!^))((?:[A-Z]|\d+)[a-z]*)`    \\ Find CamelCase
\U\1_\2                                     \\ Replace with UPPER_CASE
\L\1_\2                                     \\ Replace with lower_case
```

*Find complete filename following a /*
```
\/[A-z][^\/]+\.pst
\/()\w[^\/]+\.(...)             \\ <-- This one might capture everything?
\/()\w[^\/]+\.(sql|txt|PRC|FNC|jpg|jpeg|gif|xlsx|xls|docx|doc|pptx|ppt|csv|pdf|zip|pgp|msg|png|xml|db|vsd)
```

*No idea what this is :D*
`[A-Z])\w+\.(.*)|(~.*)\w+`

*Capture E-mail*
`message_id=<([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)>`

*Extract Fields BETWEEN two strings*
`(?<=STRING_1)(.*)(?=STRING_2)`