#!/bin/bash

echo ""

date

echo "Line Utililization - In"
snmpget -v2c -c acela 10.106.254.71 .1.3.6.1.4.1.303.3.3.19.21.6.4.4.3.0

echo "Line Utilization - Out"
snmpget -v2c -c acela 10.106.254.71 .1.3.6.1.4.1.303.3.3.19.21.6.4.4.7.0

echo "Target Rate - In"
snmpget -v2c -c acela 10.106.254.71 .1.3.6.1.4.1.303.3.3.19.21.6.4.4.11.0

echo "Target Rate - Out"
snmpget -v2c -c acela 10.106.254.71 .1.3.6.1.4.1.303.3.3.19.21.6.4.4.16.0

echo "Total WAN Rate - In"
snmpget -v2c -c acela 10.106.254.71 .1.3.6.1.4.1.303.3.3.19.21.6.4.4.12.0

echo "IPSec WAN Rate - In"
snmpget -v2c -c acela 10.106.254.71 .1.3.6.1.4.1.303.3.3.19.21.6.4.4.13.0

echo "Total WAN Rate - Out"
snmpget -v2c -c acela 10.106.254.71 .1.3.6.1.4.1.303.3.3.19.21.6.4.4.17.0

echo "IPSec WAN Rate - Out"
snmpget -v2c -c acela 10.106.254.71 .1.3.6.1.4.1.303.3.3.19.21.6.4.4.18.0

snmpget.sh (END) 
