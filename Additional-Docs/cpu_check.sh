#!/bin/bash

# Script to catch CPU and RAM Loads
#
# Cron schedule
# */1 4 * * 0 /home/tango/cpu-check.sh >> /home/tango/CPU_Check.log

# Print the date/time and current CPU Load
echo ""
echo "CPU Load"

date

ps auxr

# Print the date/time and current Free RAM, in MB with Totals
echo ""
echo "Free RAM"
date

free -motl